import shutil
import xml.dom.minidom as DOM 
import arcpy
import pandas as pd
import os
import logging
from datetime import datetime
import sys
import io
import numpy as np
import json
import urllib.request
from urllib.request import urlopen
# In[3]:

#import urllib,
import json






#Setup logging - levels are DEBUG,INFO,WARNING,ERROR,CRITICAL
logger = logging.getLogger()
fhandler = logging.FileHandler(filename=r'pathlog', mode='w')
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
fhandler.setFormatter(formatter)
logger.addHandler(fhandler)
logger.setLevel(logging.INFO)


con = r'ruta producto script conexión server'


def normalize(s):
    replacements = (
        ("á", "a"),
        ("é", "e"),
        ("í", "i"),
        ("ó", "o"),
        ("ú", "u"),
        ("Á", "A"),
        ("É", "E"),
        ("Í", "I"),
        ("Ó", "O"),
        ("Ú", "U"),
        ("Ñ", "N"),
    )
    for a, b in replacements:
        s = s.replace(a, b).replace(a.upper(), b.upper())
    return s



'''
def createSD(sddraft, sd, con):
    # stage and upload the service if the sddraft analysis did not contain errors
    # Execute StageService
    arcpy.StageService_server(sddraft, sd)
    # Execute UploadServiceDefinition
    arcpy.UploadServiceDefinition_server(sd, con)
    print("----- EL SERVICIO FUE PUBLICADO CON EXITO -------")

'''

#con = ruta de la conexión server Script ConectServer.py
def createSD(sddraft, sd, con):
    try:
        arcpy.StageService_server(sddraft, sd)
    #    arcpy.StageService_server(sddraft, sd)
    #    # Execute UploadServiceDefinition
        arcpy.UploadServiceDefinition_server(sd, con,"",
                                         "", "", "", "", "OVERRIDE_DEFINITION", "SHARE_ONLINE",
                                         "PRIVATE", "SHARE_ORGANIZATION", "")
        #arcpy.UploadServiceDefinition_server(sd, con)
        arcpy.AddMessage("EL SERVICIO FUE PUBLICADO CON EXITO")
    except Exception as stage_exception:
        arcpy.AddMessage("Errores encontrador no se pudo publicar - {}".format(str(stage_exception)))


import io 
def descriptionService(ruta):
    global foo         
    for root, dirs, files in os.walk(ruta):
        if root.endswith(".gdb"):
            next
        else:
            for file in files:
                if file.endswith(".json"):
                    with io.open(os.path.join(root, file),'r', encoding='utf-8') as file:
                        data = json.load(file,strict=False)
                        arcpy.AddMessage("{} Lectura Json".format(data))
                        summary = data['summary']
                        tags = data['tags']
                        tags = ', '.join([elem for elem in tags])
                        description = data['description']
                        creditos = data["accesinformation"]
                        return tags, summary, description, creditos
                else:
                    next
                        
                    
                    

import xml.etree.cElementTree as ET
from xml.dom import minidom

import codecs


def ChangeXML(sddraftObj,description, creditos):
    doc = DOM.parse(sddraftObj)
    # Turn off caching
    configProps = doc.getElementsByTagName('ConfigurationProperties')[0]
    propArray = configProps.firstChild
    propSets = propArray.childNodes
    for propSet in propSets:
        keyValues = propSet.childNodes
        for keyValue in keyValues:
            if keyValue.tagName == 'Key':
                if keyValue.firstChild.data == "isCached":
                    keyValue.nextSibling.firstChild.data = "false"

    # Turn on feature access capabilities
    configProps = doc.getElementsByTagName('Info')[0]
    propArray = configProps.firstChild
    propSets = propArray.childNodes
    for propSet in propSets:
        keyValues = propSet.childNodes
        for keyValue in keyValues:
            if keyValue.tagName == 'Key':
                if keyValue.firstChild.data == "WebCapabilities":
                    keyValue.nextSibling.firstChild.data = "Map,Query,Data"
                
    descriptions1 = doc.getElementsByTagName('Credits')
    for desc in descriptions1:
        if desc.parentNode.tagName == 'ItemInfo':
            # modify the Description
            if desc.hasChildNodes():
                desc.firstChild.data = creditos
            else:
                txt = doc.createTextNode(creditos)
                desc.appendChild(txt)
        descriptions = doc.getElementsByTagName('Description')
        for desc in descriptions:
            if desc.parentNode.tagName == 'ItemInfo':
                # modify the Description
                if desc.hasChildNodes():
                    desc.firstChild.data = description
                else:
                    txt = doc.createTextNode(description)
                    desc.appendChild(txt)
    keys = doc.getElementsByTagName('Key')
    for key in keys:
        if key.firstChild.data == 'UsageTimeout': key.nextSibling.firstChild.data = 60
        if key.firstChild.data == 'WaitTimeout': key.nextSibling.firstChild.data = 120
        if key.firstChild.data == 'IdleTimeout': key.nextSibling.firstChild.data = 600
        if key.firstChild.data == 'MinInstances': key.nextSibling.firstChild.data = 0
        if key.firstChild.data == 'MaxInstances': key.nextSibling.firstChild.data = 2
	#if key.firstChild.data =='schemaLockingEnabled': key.nextSibling.firstChild.data = False
	
    if os.path.exists(sddraft): os.remove(sddraft)
    #xmlstr = minidom.parseString(ET.tostring(sddraft)).toprettyxml(indent="   ", encoding='UTF-8')
    #with open(sddraft, mode='w',encoding='utf-8') as file:
    file=codecs.open(sddraft,"w","utf-8")
    
    print("abrio")
    doc.writexml(file)
    file.close()







arcpy.AddMessage("------PROCESO DE PUBLICACION INICIADO-------")
        

##SE RECORREN LAS CARPETAS QUE TIENEN LOS ARCHIVOS APRX
for root, dirs, files in os.walk(i):
    if root.endswith(".gdb"):
        next
    else:
        try:
            for file in files:
                if file.endswith(".aprx"):
                    mapDoc=os.path.join(root,"RUTA ARCHIVO"+".aprx")
                    arcpy.AddMessage("{} mapDoc:".format(mapDoc))
                    tags, resumen, description, creditos = descriptionService("PATH JSON METADATO")
                    sddraft=os.path.join(root,"RUTA ARCHIVO"+".sddraft")
                    arcpy.AddMessage("{} sddraft:".format(sddraft))
                    sd=os.path.join(root,"RUTA ARCHIVO"+".sd")
                    arcpy.AddMessage("{} Service generado:".format(sd))
                    arcpy.AddMessage("Creando Sddraft para el servicio: {}".format(mapDoc))
                    logging.info("Creando Sddraft para el servicio: {}".format(mapDoc))
                    arcpy.AddMessage("Inicia sddraft")
                    aprx=arcpy.mp.ArcGISProject("RUTA APRX")
                    m = aprx.listMaps(LayerPro[0])[0]
                    sharing_draft = m.getWebLayerSharingDraft("FEDERATED_SERVER", "MAP_IMAGE", service)
                    sharing_draft.federatedServerUrl = "URL PORTAL"
                    sharing_draft.copyDataToServer = False
                    sharing_draft.serverFolder = "NAME FOLDER SERVER"
                    sharing_draft.summary = resumen
                    sharing_draft.tags = tags
                    if os.path.exists(sddraft):
                        os.remove(sddraft)
                        sharing_draft.exportToSDDraft(sddraft)
                    else:
                        sharing_draft.exportToSDDraft(sddraft)                                   
                    arcpy.AddMessage("Modificando XML para el servicio: {}".format(mapDoc))
                    logging.info("Modificando XML para el servicio: {}".format(mapDoc))
                    ChangeXML(sddraft,description,creditos)
                    arcpy.AddMessage("Validando el servicio: {}".format(mapDoc))
                    logging.info("Validando el servicio: {}".format(mapDoc))
                    if os.path.exists(sd):
                        os.remove(sd)
                    createSD(sddraft, sd, con )

                    
                    
        except arcpy.ExecuteError:
            arcpy.AddMessage("El servicio no pudo ser publicado, revisar los errores en el servicio y eliminar la carpeta en la ruta ")
            msgs = arcpy.GetMessages(2)
            arcpy.AddMessage("Error de Arcpy:\n {}".format(msgs))
            logging.error("Error de Arcpy:\n {}".format(msgs))
            arcpy.AddMessage("Error de Arcpy:\n ".format(msgs))

        except:
            arcpy.AddMessage("El servicio no pudo ser publicado, revisar los errores en el servicio y eliminar la carpeta en la ruta ")
            exc_tuple = sys.exc_info()
            arcpy.AddMessage("Error de sistema: \n {}".format(str(exc_tuple[1])))
            logging.error("Error de sistema: \n {}".format(str(exc_tuple[1])))




        now = datetime.now()
        arcpy.AddMessage("{} - proceso Finalizado".format(now.strftime("%d/%m/%Y %H:%M:%S")))
        logging.info("proceso Finalizado")



